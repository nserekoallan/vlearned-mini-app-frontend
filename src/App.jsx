import React, {useState} from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from "./components/Home";
import Test from "./components/Test";
const App = () =>{
  const [newTestState, setNewTestState] = useState([]);
  const [activeCategoryState, setActiveCategoryState] = useState("Math");
  return (
    <Routes>
      <Route path='/' element={ <Home newTestState={newTestState} setNewTestState={setNewTestState} setActiveCategoryState={setActiveCategoryState} activeCategory={activeCategoryState}/>}></Route>

      <Route path='/test/attempt/:name' element={<Test/>}></Route>
    </Routes>        
        

  );
}

export default App;
