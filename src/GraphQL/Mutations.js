import { gql } from "@apollo/client";


//update test
// {
//     "input": {"name":"TEST 2"},
//     "connect": {
//       "questions": [
//         {
//           "where": {
//             "node": {
//               "name": "Who is the current president of Kenya?"
//             }
//           }
//         }
//       ]
//     }
//   }
// mutation Mutation($where: TestWhere, $connect: TestConnectInput) {
//     updateTests(where: $where, connect: $connect) {
//       tests {
//         name
//         questions {
//           name
//           answer
//         }
//       }
//     }
//   }
export const ADD_QUESTION = gql`
mutation createQuestions($input: [QuestionCreateInput!]!) {
  createQuestions(input: $input) {
    questions {
      name
    }
  }
}`;
// {
//   "input": [
//     {
//       "name": "test",
//       "answer": "test"
//     }
//   ]
// }
export const UPDATE_QUESTION = gql`
mutation CreateQuestions($input: [QuestionCreateInput!]!) {
  createQuestions(input: $input) {
    questions {
      name
    }
  }
}
`
export const ADD_CATEGORY = gql`
    mutation Mutation($input: [CategoryCreateInput!]!) {
  createCategories(input: $input) {
    categories {
      name
    }
  }
}
`;

export const UPDATE_TEST = gql`
mutation UpdateTests($where: TestWhere, $connect: TestConnectInput) {
  updateTests(where: $where, connect: $connect) {
    tests {
      name
      questions {
        name
      }
    }
  }
}
`;
export const ADD_TEST = gql`
mutation CreateTests($input: [TestCreateInput!]!, $connect: TestConnectInput) {
  createTests(input: $input, connect: $connect) {
    tests {
      name
    }
    questions {
      connect
    }
  }
}
`;

export const ADD_TEST_QUESTIONS = gql`
mutation CreateTests($input: [TestCreateInput!]!) {
  createTests(input: $input) {
    tests {
      name
    }
  }
}

`;