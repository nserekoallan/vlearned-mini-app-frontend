import { gql } from "@apollo/client";

export const GET_QUESTIONS = gql`
    query Questions {
        questions {
            name
            }
        }`;

export const GET_CATEGORY_QUESTIONS = gql`
        query Questions($where: QuestionWhere) {
        questions(where: $where) {
            name
        }
        }`;

// export const GET_CATEGORY_QUESTIONS = gql`
//   query Questions($where: QuestionWhere) {
//     questionsConnection(where: $where) {
//     edges {
//       node {
//         name
//       }
//     }
//   }
// }
// `

export const GET_SINGLE_TEST = gql`
query Tests($where: TestWhere) {
  tests(where: $where) {
    name
    questions {
      name
      answer
    }
  }
}
`
export const GET_CATEGORIES = gql`
    query Categories {
        categories {
          name
        }
        }
        `;
export const GET_TESTS = gql`
query Tests {
    tests {
      name
    }
    }
    `;
