import  React, { useState } from "react";
import { AddItemButton } from "../styles";
import NewCategoryForm from "./NewCategoryForm";
// import { useAppState } from "../context/AppStateContext";
import { useMutation } from "@apollo/client";
import { ADD_CATEGORY } from "../GraphQL/Mutations";


const AddNewCategory = (props) =>{
    const [showForm, setShowForm] = useState(false);
    const { toggleButtonText, dark } = props;
    // const {state} = useAppState();
    const [onAdd, {loading, error, data}] = useMutation(ADD_CATEGORY);
    const toggleForm = (status) =>{
        setShowForm(status);
    }
 
    if(showForm){
        return (
            <NewCategoryForm toggleForm={toggleForm}/>
        );
        }
    return (
        <AddItemButton dark={dark} onClick={()=>setShowForm(true)}>
            {toggleButtonText}
        </AddItemButton>
    )
}

export default AddNewCategory;