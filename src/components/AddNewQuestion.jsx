import  React, { useState } from "react";
import { AddItemButton } from "../styles";
import NewQuestionForm from "./NewQuestionForm";
// import { useAppState } from "../context/AppStateContext";
import { useMutation } from "@apollo/client";
import { ADD_CATEGORY } from "../GraphQL/Mutations";


const AddNewQuestion = (props) =>{
    const [showForm, setShowForm] = useState(false);
    const { toggleButtonText, dark, reloadQn } = props;
    // const {state} = useAppState();
    const toggleForm = (status) =>{
        setShowForm(status);
    }
 
    if(showForm){
        return (
            <NewQuestionForm reloadQn={reloadQn} toggleForm={toggleForm}/>
        );
        }
    return (
        <AddItemButton dark={dark} onClick={()=>setShowForm(true)}>
            {toggleButtonText}
        </AddItemButton>
    )
}

export default AddNewQuestion;