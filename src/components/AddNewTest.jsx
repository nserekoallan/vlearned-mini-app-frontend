import  React, { useState } from "react";
import { AddItemButton } from "../styles";
import NewTestForm from "./NewTestForm";
// import { useAppState } from "../context/AppStateContext";

const AddNewTest = (props) =>{
    const [showForm, setShowForm] = useState(false);
    const { toggleButtonText, dark } = props;
    const toggleForm = (status) =>{
        setShowForm(status);
    }
 
    if(showForm){
        return (
            <NewTestForm toggleForm={toggleForm} setNewTest={props.setNewTest}/>
        );
        }
    return (
        <AddItemButton dark={dark} onClick={()=>setShowForm(true)}>
            {toggleButtonText}
        </AddItemButton>
    )
}

export default AddNewTest;