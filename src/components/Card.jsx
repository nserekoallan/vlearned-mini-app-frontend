import React from "react";
import { CardContainer } from "../styles";

const Card = ({text}) => {
    return <CardContainer>{text}</CardContainer>
}

export default Card;
