import React, { useRef, useEffect, useState, useCallback } from "react";
import { ColumnContainer, ColumnTitle } from "../styles";
import { useAppState } from "../context/AppStateContext";
import AddNewCategory from "./AddNewCategory";
import Card from "./Card";
import { useDrop, useDrag } from "react-dnd";
import { isHidden } from "../utils/isHidden";
import {useQuery, useLazyQuery, useMutation} from "@apollo/client";
import {GET_CATEGORY_QUESTIONS, GET_CATEGORIES, GET_TESTS} from "../GraphQL/Queries";
import AddNewQuestion from "./AddNewQuestion";
import AddNewTest from "./AddNewTest";
import QuestionCard from "./QuestionCard";
import { useNavigate, Link } from "react-router-dom";
import { ADD_TEST, UPDATE_TEST, ADD_TEST_QUESTIONS } from "../GraphQL/Mutations";
import { AddItemButton } from "../styles";

const Column = ({text, setNewTest, setActiveCategory, activeCategory, toggleTest}) =>{

    // const { state, dispatch } = useAppState();
    const [reloadTestsState, setReloadTestsState] = useState(false);

    const itemTypes = {
        CARD: "card",
        TEST: "test"
    }
    const ref = useRef(null);
    const dragRef = useRef(null);

    switch(text){
        case "Categories":{
            const {loading, error, data} = useQuery(GET_CATEGORIES);
            /**Set initial active category to the first item in Categories */ 
            const categoryData = data
          
            return (
                <ColumnContainer >
                    <ColumnTitle>{text}</ColumnTitle>
                    {data&&
                        <select onChange={e=>setActiveCategory(e.target.value)}>
                        { data.categories.map( (category, i) => (
                            <option value={category.name} ><Card text={category.name}   key={i}/> </option>
                        ))}
                        </select>
                    }
                   
                <AddNewCategory toggleButtonText='+ Add category' dark/>

                </ColumnContainer>
            );}
          
        case "Questions":{
                const [reloadQnState, setReloadQnState] = useState(true);
                const toggleQnState = () => {
                    reloadQnState == true? setReloadQnState(false) : setReloadQnState(true)
                }
                const [loadQuestions, {called, loading, data}] = useLazyQuery(GET_CATEGORY_QUESTIONS, {
                    variables: {variables:{
                        "where": {
                          "category": {
                            "name": activeCategory
                          }
                        }
                      }
                    }});
                useEffect(()=>{
                    loadQuestions()
                    
                }, [reloadQnState]) 
               
            return(
                <ColumnContainer>
                    <ColumnTitle>{text}</ColumnTitle>
                    {data && data.questions.map( (question, i) => (
                        <QuestionCard text={question.name} key={i} onHover={(e)=>console.log(e)}/>
                    ))}
                <AddNewQuestion toggleButtonText='+ Add question' reloadQn={toggleQnState} dark/>

            </ColumnContainer>
            )
        }
            
        case "Tests":
            const navigate = useNavigate();
            const [loadTests, {called, loading, data}] = useLazyQuery(GET_TESTS);
            useEffect(()=>{
                loadTests()
            }, [reloadTestsState]) 

            return(
                <ColumnContainer>
                    <ColumnTitle>{text}</ColumnTitle>
                    {data && data.tests.map((test, i) => (
                        <Link to={"/test/attempt/" + test.name} ><Card text={test.name} key={i} /></Link>
                    ))}
                <AddNewTest setNewTest={setNewTest} toggleButtonText='+ Add test' dark/>

                </ColumnContainer>
            )
    
        default:{
            const [testQuestions, setTestQuestions] = useState([]);         
            const [onAddTest, {loading, error, data}] = useMutation(ADD_TEST_QUESTIONS);
            const [ {isOver}, drop ] = useDrop(
                () => ({
                    accept: itemTypes.CARD,
                    drop: (item) => setTestQuestions((prevState)=>{
                        const qn = item.text;
                        const filtered = prevState.filter(question => question === qn);
                        if(filtered.length > 0){
                            alert("Question Already Exists");
                            return prevState;
                        }
                        if(prevState.length >= 3){
                            alert("Maximum reached for test!!");
                            return prevState
                        }
                        return [...prevState, qn ]
                    }),
                    collect: (monitor) => ({
                        isOver: !!monitor.isOver()
                    })
                })
            )
              return(
                <ColumnContainer ref={drop} >
                    <ColumnTitle>{text}</ColumnTitle>
                    <p>Drop Questions Here</p>
                    {testQuestions.length > 0 && testQuestions.map((qn, i) => (
                        <Card text={qn} key={i}  />
                    ))}
            <AddItemButton dark={true} onClick={()=>{if(window.confirm("Save Test")){
                if(testQuestions.length == 3){
                    onAddTest({
                        variables:{
                            "input": [
                              {
                                "questions": {
                                  "connect": [
                                    {
                                      "where": {
                                        "node": {
                                          "name_IN": testQuestions
                                        }
                                      }
                                    }
                                  ]
                                },
                                "name": text
                              }
                            ]
                          }
                    });
                    setReloadTestsState(prevState => {
                        if(prevState){
                            return false;
                        } else{
                            return true
                        }
                    })
                } else{
                    alert("Questions below minimum (3)!")
                }
     }
             toggleTest([])}}>
                    Save
             </AddItemButton>               
             </ColumnContainer>
            )
                  
        }

             }

   
}

export default Column;