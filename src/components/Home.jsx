import React from "react";
import Column from './Column';

const Home = ({newTestState, setNewTestState, setActiveCategoryState, activeCategory}) =>{
    return (
    <>
    <Column text={"Categories"} setActiveCategory = {setActiveCategoryState}/>
      <Column text={"Questions"} activeCategory={activeCategory}/>
      <Column  text={"Tests"} setNewTest={setNewTestState}/>
      { newTestState.length > 0 && 
      <Column className="test" text={newTestState[0].name} toggleTest={setNewTestState}/>
      } 
    </>
    )
}

export default Home;