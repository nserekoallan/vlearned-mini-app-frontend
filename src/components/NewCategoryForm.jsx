import React, {useState} from "react";
import { NewItemFormContainer, NewItemButton, NewItemInput} from "../styles";
import { useFocus } from "../utils/useFocus";
import { useMutation } from "@apollo/client";
import { ADD_CATEGORY } from "../GraphQL/Mutations";

const NewCategoryForm = (props) =>{
    const [text, setText] = useState("");
    const [onAdd, {loading, error, data}] = useMutation(ADD_CATEGORY);
    const inputRef = useFocus();

    return(
        <NewItemFormContainer>
            <NewItemInput ref={inputRef} value={text} onChange={e => setText(e.target.value)}/>
            <NewItemButton onClick={ () =>{
                if(text !== "" && text !=="Category here"){
                    onAdd({variables:{
                        "input": [
                            {
                            "name": text
                            }
                        ]
                        }});
                }
            
                props.toggleForm(false);
            }
}>
                Create
            </NewItemButton>

        </NewItemFormContainer>
    )
}

export default NewCategoryForm;
