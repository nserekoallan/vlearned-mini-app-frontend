import React, {useState} from "react";
import { NewItemFormContainer, NewItemButton, NewItemInput} from "../styles";
import { useMutation } from "@apollo/client";
import { ADD_QUESTION } from "../GraphQL/Mutations";

const NewQuestionForm = (props) =>{
    const [question, setQuestion] = useState("Question here");
    const [answer, setAnswer] = useState("Answer here");
    const [onAdd, {loading, error, data}] = useMutation(ADD_QUESTION);

    return(
        <NewItemFormContainer>
            <NewItemInput value={question} onChange={e => setQuestion(e.target.value)}/>
            <NewItemInput value={answer} onChange={e => setAnswer(e.target.value)}/>
            <NewItemButton onClick={ () =>{
                if(question && question !== "" && question !== "Question here"){
                  if(window.confirm("Save Question")){
                    onAdd({variables:{
                      "input": [
                        {
                          "name": question,
                          "answer": answer,
                          "category": {
                            "connect": {
                              "where": {
                                "node": {
                                  "name": "Math"
                                }
                              },
                              "overwrite": false
                            }
                          }
                        }
                      ]
                    }})
                  }
                }
                /**Refetch questions from database */
                props.reloadQn();
                props.toggleForm(false);
            }
}>
                Create
            </NewItemButton>

        </NewItemFormContainer>
    )
}

export default NewQuestionForm;
