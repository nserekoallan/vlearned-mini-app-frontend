import React, {useState} from "react";
import { NewItemFormContainer, NewItemButton, NewItemInput} from "../styles";
import { useFocus } from "../utils/useFocus";

const NewTestForm = (props) =>{
    const [text, setText] = useState("Test Name");
    const inputRef = useFocus();

    return(
        <NewItemFormContainer>
            <NewItemInput ref={inputRef} value={text} onChange={e => setText(e.target.value)}/>
            <NewItemButton onClick={ () =>{
                if(text !== "" && text !=="Test Name"){
                    props.setNewTest([{name:text}]);
                }
            
                props.toggleForm(false);
            }
}>
                Create
            </NewItemButton>

        </NewItemFormContainer>
    )
}

export default NewTestForm;
