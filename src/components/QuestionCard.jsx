import React from "react";
import { useDrag } from "react-dnd";
import { CardContainer } from "../styles";

const QuestionCard = ({text}) => {
    const [{ isDragging }, drag] = useDrag(() => ({
        type:"card",
        item: { text }
,        collect: (monitor) => ({
            isDragging: !!monitor.isDragging()
        })
      }))


    return <CardContainer ref={drag} style={{
        opacity: isDragging ? 0 : 1,
        cursor: 'drag',
      }} >{text}</CardContainer>
}

export default QuestionCard;
