import React, { useCallback,useEffect, useState } from 'react';
import ReactFlow, { useEdgesState, addEdge, Handle, Position } from 'react-flow-renderer';
import {GET_SINGLE_TEST} from "../GraphQL/Queries";
import { useLazyQuery } from '@apollo/client';
import { useNavigate, useParams } from 'react-router-dom';
import { TestCardContainer, TestTitle } from '../styles';

const Test = () => {
    const [initialNodes, setInitialNodes] = useState([]);
    const formNodes = (test) => {
      let questions = [];
      const filtered = test.tests.filter((test)=> test.name == name);
      let qn_y = 150;
      let ans_y = 150
        filtered[0].questions.map((question)=>{
          questions.push({id: question.name, type:'question', position: {x:  0, y: qn_y}, data:{label: question.name, target: question.answer}});
            questions.push({id: question.answer, type:'answer', position: {x: 250, y: ans_y}, data:{label: question.answer, source: question.name}});
          qn_y += 100;
          ans_y += 100;
      })
      setInitialNodes(questions);
      }

      const validateConnection = (conn) =>{
        const subNode = initialNodes.filter((question) => (question.data.label == conn.source && question.data.target == conn.target));
        if(subNode.length > 0) return true;
        return false;
      }
      
      const isValidConnection = (connection) => validateConnection(connection) ;
      // const onConnectStart = (_, { nodeId, handleType }) =>
      //   console.log('on connect start', { nodeId, handleType });
      // const onConnectEnd = (event) => console.log('on connect end', event);
      
      const Question = ({data}) => (
        <>
          <TestCardContainer>{data.label}</TestCardContainer>          
          <Handle type="source" position={Position.Right} isValidConnection={isValidConnection} />
        </>
      );
      
      const Answer = ({ data}) => (
        <>
          <Handle type="target" position={Position.Left} isValidConnection={isValidConnection} />
          <TestCardContainer>{data.label}</TestCardContainer>          
        </>
      );
      
      const shuffleQuestions = (test) => {
          let currentIndex = test.length,  randomIndex;
        
          // While there remain elements to shuffle.
          while (currentIndex != 0) {
        
            // Pick a remaining element.
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;
        
            // And swap it with the current element.
            [test[currentIndex], test[randomIndex]] = [
              test[randomIndex], test[currentIndex]];
          }
        
          return test;
        }
        
      
      const nodeTypes = {
        question: Question,
        answer: Answer,
      };
   

    const navigate = useNavigate();

    /**Get test name from route */
    const { name } = useParams();
    if(!name){
        alert("No Test Selected, redirecting...");
        navigate("/")

    };
    /**Fetch questions in the test from database */
    const [loadTest, {called, loading, data}] = useLazyQuery(GET_SINGLE_TEST, {
        variables: { variables:{
                "where": {
                      "name":name
                  }
            }} 
})

useEffect(()=>{
  if(!called){
    loadTest()
  }
  if(data){
    formNodes(data);
  }

},[loading, data])

    //  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
     const [edges, setEdges, onEdgesChange] = useEdgesState([]);
     
  const onConnect = useCallback((params) => setEdges((els) => addEdge(params, els)), []);
  if(initialNodes.length > 0){
    return (
      <>
      <ReactFlow
        nodes={initialNodes}
        edges={edges}
        onEdgesChange={onEdgesChange}
        onConnect={onConnect}
        selectNodesOnDrag={false}
        className="validationflow"
        nodeTypes={nodeTypes}
        fitView
        attributionPosition="bottom-left"
      />
      </>
     
    );
  } else{
    return (
      <>
      <p>Loading...</p>
      </>
      

    )
  }
      
  
}
  

export default Test;
