import React, {createContext, useReducer, useState, useContext, useEffect } from "react";
import { moveItem } from "../drag/moveItem";
import { findItemIndexById } from "../utils/findItemIndexById";
import {v1 as uuid} from 'uuid';
import {GET_CATEGORIES} from '../GraphQL/Queries';
import { useQuery, gql } from "@apollo/client";


const AppStateContext = createContext({});
// const fetchCategories = () =>{
//      return useQuery(GET_CATEGORIES);
// }

const appStateReducer = (state, action) => {
    switch (action.type){
        case "FETCH_CATEGORIES":
         return { 
            ...state
         }
        case "ADD_TEST": {
            /** Reducer logic here */
            state.newTest.push(action.payload);
            return {
                ...state
            }
        }
        case "ADD_TASK": {
            /** Reducer logic here */ 
            const targetLaneIndex = findItemIndexById(
                state.lists, action.payload.taskId
            );

            state.lists[targetLaneIndex].tasks.push({
                id: uuid(),
                text: action.payload.text
            })

            return {
                ...state
            }
        } 
        case "MOVE_QUESTION":{
            const {
                dragIndex,
                hoverIndex,
                sourceColumn,
                targetColumn
                } = action.payload ;
                const item = state.questions[dragIndex].splice(dragIndex,1)[0]
                state.newTest[0].splice(hoverIndex, 0, item)        
                return { ...state }
        }
        case "SET_DRAGGED_ITEM":{
            return {
                ...state, draggedItem: action.payload 
            }
        }
        default: {
            return state
        }
    }
}



export const AppStateProvider = ({ children }) => {
    // useEffect(()=>{
    //     /**Fetch categories */
    //     const {loading, error, data} = useQuery(GET_CATEGORIES); 
        
    //     if(data){
    //       setCategoryState(data);
    //     }
    //   })
    // const {loading, error, data} = useQuery(GET_CATEGORIES); 
    // console.log(loading)

      const {categoryState, setCategoryState} = useState([]);
      const {testState, setTestState} = useState([]);
      const {questionState, setQuestionState} = useState([]);
      const {newTestState, setNewTestState} = useState([]); 
      if(categoryState && categoryState.length > 0){
        const {activeCategoryState, setActiveCategoryState} = useState(categoryState[0]);
      }
    const appData = {};
     appData.categories = categoryState;
    appData.tests = testState;
    appData.questions = questionState;
    appData.newTest =  newTestState;
    appData.activeCategory = categoryState;

    const [state, dispatch] = useReducer(appStateReducer, appData);

    return (
        <AppStateContext.Provider value={{state, dispatch}}>
            {children}
        </AppStateContext.Provider>
    )
}

export const useAppState = () => {
    return useContext(AppStateContext);
}

