import { useDrag } from "react-dnd";
import { useAppState } from "../context/AppStateContext";

export const useItemDrag = (item) => {
    const { dispatch } = useAppState();
    const [  , drag ] = useDrag({
        type: item.type,
         item: () =>{
            return [item, 
                dispatch({
                    type: "SET_DRAGGED_ITEM",
                    payload: item
                })
            ]
         } ,
        end: () => dispatch({ type: "SET_DRAGGED_ITEM", payload: undefined }),
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        })
    })

    return { drag }
}