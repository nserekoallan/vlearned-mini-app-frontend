import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import 'reactflow/dist/style.css';
import App from './App';
import {AppStateProvider}  from "./context/AppStateContext";
import { DndProvider } from 'react-dnd';
import {HTML5Backend}from "react-dnd-html5-backend";
import {ApolloClient, InMemoryCache, ApolloProvider} from "@apollo/client";
import { AppContainer } from './styles';
import { BrowserRouter } from 'react-router-dom';

const client = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache(),
  queryDeduplication: false,
});

const root = ReactDOM.createRoot(
  document.getElementById('root')
);

root.render(
    <DndProvider backend={HTML5Backend}>

  <BrowserRouter>
  <ApolloProvider client={client}>

    <AppContainer>
      <App />
    </AppContainer>
    </ApolloProvider>
</BrowserRouter>
</DndProvider>


);
