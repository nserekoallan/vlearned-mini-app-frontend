import { DragItem } from "../drag/DragItem";

export const isHidden = (
    draggedItem,
    itemType,
    id
) => {
    return Boolean(
            draggedItem && draggedItem.type === itemType && draggedItem.id === id
    );
}